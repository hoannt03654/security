CommonFunction = (function () {
    function Common_Function() {
        this.confirm = function (elem) {
            var self = $(elem);
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    self.closest('form').submit();
                } else {
                }
            }).catch(swal.noop);
        }
    }

    return new Common_Function();
});
commonFunc = CommonFunction();

