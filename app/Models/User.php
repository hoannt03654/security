<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hash;

class User extends Model
{
    protected $table = 'users';

    public static function getAll()
    {
        $value = User::where('status', '=', true)
            ->orderBy('name', 'ASC')
            ->get();
        return $value;
    }

    public static function storeInstance($request)
    {
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make(get_const('HASH_DEFAULT'));
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
            $file->move(get_const('USER_UPLOAD'), $name);
            $user->image = $name;
        }
        $user->save();
    }

    public static function updateInstance($request, $user)
    {
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if ($request->hasFile('image')) {
            $image = public_path(get_const('USER_UPLOAD') . $user->image);
            if (strlen($user->image) > 0 && file_exists($image)) {
                unlink($image);
            }
            $file = $request->file('image');
            $fileName = $file->getClientOriginalName();
            $name = md5(($fileName) . date('Y-m-d H:i:s')) . '.' . $file->getClientOriginalExtension();
            $file->move(get_const('USER_UPLOAD'), $name);
            $user->image = $name;
        }
        $user->save();
    }
}
