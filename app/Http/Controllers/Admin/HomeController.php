<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HomeRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::getAll();

        return view('admin.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(HomeRequest $request)
    {
        User::storeInstance($request);

        return redirect()->route('admin.home')->with([
            'message' => 'Created user successfully!',
            'level' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);

        return view('admin.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(HomeRequest $request, $id)
    {
        $user = User::findOrFail($id);

        User::updateInstance($request, $user);

        return redirect()->route('admin.home')->with([
            'message' => 'Updated user successfully!',
            'level' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if (Auth::user()->id == $user->id) {
            return back()->with(
                [
                    'level' => 'error',
                    'error' => 'Không cho phép xóa tài khoản đăng nhập hiện tại !'
                ]);
        } else {
            $user->status = get_const('ACTIVE_OFF');
            $user->update();

            return back()->with(['level' => 'success', 'message' => 'Deleted user successfully.']);
        }
    }
}
