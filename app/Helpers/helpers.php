<?php

use App\Helpers\Facades\Tool;

if (! function_exists('get_const')) {
    function get_const($key, $all = false)
    {
        return Tool::getConst($key, $all);
    }
}

if (! function_exists('active_link')) {
    function active_link($route)
    {
        return Tool::activeLink($route);
    }
}

if (! function_exists('get_image')) {
    function get_image($path, $mode = null)
    {
        return Tool::getImage($path, $mode);
    }
}

if (! function_exists('get_sort')) {
    function get_sort($col, $display = null)
    {
        return Tool::getSortableColumn($col, $display);
    }
}

if (! function_exists('sanitize_query')) {
    function sanitize_query($query)
    {
        return Tool::sanitizeQuery($query);
    }
}

if (! function_exists('highlight_search')) {
    function highlight_search($content, $keyword)
    {
        return Tool::highlight($content, $keyword);
    }
}

if (! function_exists('class_error')) {
    function class_error($name, $errors, $tab = null)
    {
        return Tool::classError($name, $errors, $tab);
    }
}

if (! function_exists('document_set_status')) {
    function document_set_status($status)
    {
        return Tool::getDocumentSetStatus($status);
    }
}

if (! function_exists('document_status')) {
    function document_status($status)
    {
        return Tool::getDocumentSetStatus($status);
    }
}

if (! function_exists('get_page_offset')) {
    function get_page_offset($collection)
    {
        return Tool::getPageOffset($collection);
    }
}

if (! function_exists('recursive_render')) {
    function recursive_render($dir, $items, $status = false)
    {
        return Tool::recursiveRender($dir, $items, $status);
    }
}

if (! function_exists('check_exist_attachment')) {
    function check_exist_attachment($file, $type = null)
    {
        return Tool::checkExistAttachment($file, $type);
    }
}

if (! function_exists('parse_version')) {
    function parse_version($ver)
    {
        return Tool::parseVersion($ver);
    }
}

if (! function_exists('make_save_path')) {
    function make_save_path($doc, $type = 0)
    {
        return Tool::makeSavePath($doc, $type);
    }
}

if (! function_exists('can_approve')) {
    function can_approve($id)
    {
        return Tool::canApprove($id);
    }
}

if (! function_exists('get_link_from_path')) {
    function get_link_from_path($path)
    {
        return Tool::getLinkFromPath($path);
    }
}

if (! function_exists('get_list_pid')) {
    function get_list_pid($str)
    {
        return Tool::getListPid($str);
    }
}

if (! function_exists('parse_code')) {
	function parse_code($code)
	{
		return Tool::parseCode($code);
	}
}
