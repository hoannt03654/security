<?php

use App\Helpers\DMS_Const;

if (!class_exists('DMS_Const_Register')) {
    class DMS_Const_Register extends DMS_Const
    {
        const ACTIVE_ON = 1;
        const ACTIVE_OFF = 0;
        const DEFAULT_PAGE = 1;
        const VERSION_JS = '1';
        const HASH_DEFAULT = '123456';
        const USER_UPLOAD = 'upload/images/users/';
        const USER_BLANK = 'public/upload/images/users/blank.png';
        const ALLOWED_DOC_EXT = ['doc', 'docx', 'txt', 'xls', 'xlsx', 'pdf'];
        const ALLOWED_DOC_MIMES = 'doc,DOC,docx,DOCX,txt,TXT,xls,XLS,xlsx,XLSX,pdf,PDF';
        const DEFAULT_VERSION = '1';
    }
}
